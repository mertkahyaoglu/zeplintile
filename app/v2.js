const images = {
    1024: 'https://cdn.zeplin.io/5b5982a32e22c9d548534490/screens/7437D347-BC63-4159-8ECA-1263B5E96E27.png',
    4096: 'https://cdn.zeplin.io/5b5982a32e22c9d548534490/screens/BFB13D80-99FD-4A67-9675-2DE00986B095.png',
    8192: 'https://cdn.zeplin.io/5b5982a32e22c9d548534490/screens/07040490-862C-4570-B306-86C80D862DFD.png',
};

const selectedSize = 4096;
const tileWidthRatio = 16;
const imageUrl = images[selectedSize];

const imageWidth = selectedSize;
const imageHeight = selectedSize;

const tileWidth = imageWidth / tileWidthRatio;
const tileHeight = imageHeight / tileWidthRatio;

const getRowCount = (imageWidth, tileWidth) =>
    Math.ceil(imageWidth / tileWidth);

const getColumnCount = (imageHeight, tileColumnCount) =>
    Math.ceil(imageHeight / tileColumnCount);

const getTileCoordinates = (rowIndex, columnIndex, tileWidth, tileHeight) => {
    const coords = { sx: rowIndex * tileWidth, sy: columnIndex * tileHeight };
    return coords;
};

const createCanvas = (width, height) => {
    const canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    return canvas;
};

const renderTiles = (image, fullWidth, fullHeight, tileWidth, tileHeight) => {
    performance.mark('draw-start');
    const canvas = createCanvas(tileWidth, tileHeight);
    const context = canvas.getContext('2d');

    const rowCount = getRowCount(fullWidth, tileWidth);
    const columnCount = getColumnCount(fullHeight, tileHeight);

    const columns = document.createElement('div');
    for (let columnIndex = 0; columnIndex < columnCount; columnIndex++) {
        const row = document.createElement('div');
        row.classList.add('imageRow');

        for (let rowIndex = 0; rowIndex < rowCount; rowIndex++) {
            const coords = getTileCoordinates(
                rowIndex,
                columnIndex,
                tileWidth,
                tileHeight
            );

            context.drawImage(
                image,
                coords.sx,
                coords.sy,
                tileWidth,
                tileHeight,
                0,
                0,
                tileWidth,
                tileHeight
            );

            const img = document.createElement('img');
            img.src = canvas.toDataURL('image/png');
            row.appendChild(img);
        }

        columns.appendChild(row);
    }

    document.body.appendChild(columns);

    performance.mark('draw-end');
    performance.measure('draw', 'draw-start', 'draw-end');
    const measure = performance.getEntriesByName('draw')[0];
    console.log(
        `Size: ${selectedSize}, Divider: ${tileWidthRatio}\nDrawing tiles took ${
            measure.duration
        } milliseconds.`
    );
    performance.clearMeasures();
};

const image = new Image(tileWidth, tileHeight);
image.src = imageUrl;
image.setAttribute('crossOrigin', 'anonymous');
image.onload = () =>
    renderTiles(image, imageWidth, imageHeight, tileWidth, tileHeight);
