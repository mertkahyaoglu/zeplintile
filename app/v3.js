function arrayBufferToBase64(buffer) {
    const binary = '';
    const bytes = [].slice.call(new Uint8Array(buffer));
    bytes.forEach(b => (binary += String.fromCharCode(b)));
    return window.btoa(binary);
}

fetch('/30kx24k.png').then(response => {
    response.arrayBuffer().then(buffer => {
        const base64Flag = 'data:image/jpeg;base64,';
        const imageStr = arrayBufferToBase64(buffer);

        const image = document.createElement('img');
        image.src = base64Flag + imageStr;
        document.body.appendChild(image);
    });
});
