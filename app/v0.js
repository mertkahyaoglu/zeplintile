const images = {
    4096: 'https://cdn.zeplin.io/5b5982a32e22c9d548534490/screens/BFB13D80-99FD-4A67-9675-2DE00986B095.png',
    8192: 'https://cdn.zeplin.io/5b5982a32e22c9d548534490/screens/07040490-862C-4570-B306-86C80D862DFD.png',
};

const selectedSize = 4096;
const imageUrl = images[selectedSize];

performance.mark('draw-start');
const image = new Image();
image.src = imageUrl;
document.body.appendChild(image);

performance.mark('draw-end');
performance.measure('draw', 'draw-start', 'draw-end');
const measure = performance.getEntriesByName('draw')[0];
console.log(
    `Size: ${selectedSize}\nDrawing tiles took ${
        measure.duration
    } milliseconds.`
);
performance.clearMeasures();
